import sys

import PIL
from PIL import Image, ImageDraw, ImageFont


def image_from_string(text:str,fontname:str,char_size_px:int,columns:int=0)->Image.Image:
	assert char_size_px>0,"Character size must be greater than zero."
	columns=len(text) if columns<=0 else columns
	rows:int=len(text)//columns+1
	width:int=columns*char_size_px
	height:int=char_size_px*rows
	image:Image.Image=Image.new("RGBA",(width,height),(255,255,255,0))
	font:ImageFont.FreeTypeFont=ImageFont.truetype("C:/Windows/Fonts/"+fontname,char_size_px-16,encoding='unic')
	draw:ImageDraw.ImageDraw=ImageDraw.Draw(image)
	ifix:int=0 # Skip problem character
	for i,character in enumerate(text):
		i=i+ifix
		x,y=(i%columns)*char_size_px,(i//columns)*char_size_px
		bb=font.getbbox(character)
		wh=(bb[2]-bb[0],bb[3]-bb[1])
		if wh[1]==0:
			ifix-=1
			continue
		bb=( (char_size_px-wh[0])/2,(char_size_px-wh[1])/2+8,wh[0],wh[1])
		x,y=tuple([a+b for a,b in zip( (x,y),bb[0:2])])
		draw.text((x,y),character,(0,0,0),font=font,embedded_color=True)
	return image

# def create_atlas(char_size:int,atlas_width:int,font:str,offset:tuple=(0,0),checker:bool=False)->Image: # type: ignore
# 	"""
# 	> char_size: pixel size of a character.
# 	> atlas_width: number of characters per line.
# 	> font: font to use.
# 	> offset: character offset in pixels, tuple (x,y).
# 	> checker: draw checkerboard pattern.
# 	< PIL.image
# 	"""
# 	assert char_size>0,"char_size must be greater than zero."
# 	assert atlas_width>0,"width must be greater than zero."

# 	chars=tuple(chr(x) for x in range(ord("😉")-200,ord("😉")+365))
# 	chars_str=''.join(chars)

# 	width=atlas_width*char_size

# 	height=(len(chars)//atlas_width+1)*char_size
# 	img=Image.new("RGBA",(width,height),(255,255,255,0))
# 	font=ImageFont.truetype("C:/Windows/Fonts/"+font,char_size-16,encoding='unic')
# 	imgdraw=ImageDraw.Draw(img)
# 	for i,c in enumerate(chars):
# 		x=i%atlas_width*char_size
# 		y=i//atlas_width*char_size
# 		if checker and (i+(1+atlas_width%2)*(i//atlas_width))%2==0:
# 			imgdraw.rectangle((x,y,x+char_size-1,y+char_size-1),(127,127,127))
# 		imgdraw.text((x+offset[0],y+offset[1]),c,(0,0,0),font=font,embedded_color=True)
# 	return img

if __name__=="__main__":
	# !"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}~
	if len(sys.argv)<2 and False:
		print("You must provide a text string to convert.")
		exit(1)
	try:
		text=sys.argv[1]
	except IndexError:
		text=r"🚪🗝️🔑🔒💀☠️🕷️🕸️🦂🐍🐉👻🐱🐭🐰🐷🐯🪙⭐🎁💰💎🥇🏆🗡️🧪⚕️⏳⚗️🔮🔥💣🧨🪶🥾❤️💥"
	img=image_from_string(text,"seguiemj.ttf",64,8)
	#img=create_atlas(64,16,"seguiemj.ttf",(0,16),False)
	img.show()
	img.save("result.png",optimize=True)


